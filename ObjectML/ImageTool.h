//
//  ImageTool.h
//  ObjectML
//
//  Created by Quoc Khai on 6/25/17.
//  Copyright © 2017 Quoc Khai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageTool : NSObject

+ (CVPixelBufferRef)pixelBufferFromImage:(UIImage *)image;

@end
